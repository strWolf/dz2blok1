//
//  MyFunc.swift
//  DZ2Blok1
//
//  Created by student08 on 08.10.17.
//  Copyright © 2017 student04. All rights reserved.
//

import UIKit

class MyFunc: UITableViewController {
    
    //Проверить, является ли    заданное число совершенным и найти их
    static func numberPerfect(number: Int){
        var countDivisor = 0
        var printDivisor: String = "Делители числа:"
        for i in 1..<number{
            if number%i == 0 {
                countDivisor = countDivisor + i
                printDivisor = printDivisor + "\(i) "
            }
        }
        if countDivisor == number{
            print("Число \(number) совершенно ")
            print(printDivisor)
        }
        else{
            print("Число \(number) не совершенно ")
        }
    }
    
    
    //Подсчитать общее количество    делителей числа и вывести их
    static func countDivisorNumber(number: Int){
        var countDivisor = 0
        var printDivisor: String = "Делители числа:"
        for i in 1..<number{
            if number%i == 0 {
                countDivisor = countDivisor + 1
                printDivisor = printDivisor + "\(i) "
            }
        }
        print("Число \(number) имеет ",countDivisor)
        print(printDivisor)
    }
        
    // Вывести на экран все числа до    заданного и в обратном порядке до 0
    static func displayNumber(number: Int){
        for i in 0..<number+1{
            print(i)
        }
        for i in 0..<number+1{
            print(number-i)
        }
    }
    
    //Вывести на экран квадрат и    куб числа
    static func cubeNumber(number: Int){
        var numberPrint = 0
        numberPrint = number * number
        print("квадрат числа \(number) =", numberPrint)
        numberPrint = numberPrint * number
        print("куб числа \(number) =", numberPrint)
    }
    
    
    //сравнение 2 чисел и вывод на экран большего
    static func comparisonTwoNumbers(firstNumber: Int, secondNumber: Int){
        if firstNumber > secondNumber{
            print("число = \(firstNumber) больше")
        }
        else if firstNumber < secondNumber{
            print("число = \(secondNumber) больше")
        }
        else{
            print("числа  \(firstNumber) == \(secondNumber)   равны")
            
        }
    }
    
    
}
